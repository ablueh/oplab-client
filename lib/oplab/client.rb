require "oplab/client/version"
require "rest_client"
require "json"

module Oplab
  module Client

    @@key = nil

    def self.key=(key)
      @@key = key
    end

    def self.key
      @@key
    end

    def self.access_token(client_key, user_id, params = {})
      raise "Missing parameters" unless client_key && user_id

      #response = RestClient.post 'https://www.bitbolsa.com.br/users/authorize', {key: client_key, id: user_id}.merge(params)
      response = RestClient.post 'https://www.oplab.com.br/users/authorize', {key: client_key, id: user_id}.merge(params)

      raise "HTTP Error: unexpected status code #{response.code}" unless response.code == 200

      data = JSON.parse(response.body)

      raise "Server error: #{data['error']}" if data['error']

      data['access_token']
    end  

  end
end
