# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'oplab/client/version'

Gem::Specification.new do |spec|
  spec.name          = "oplab-client"
  spec.version       = Oplab::Client::VERSION
  spec.authors       = ["Alexandre Amaral"]
  spec.email         = ["alexandre@bitbolsa.com.br"]
  spec.description   = %q{A set of helper methods for requesting Oplab services such as authentication}
  spec.summary       = %q{Access to Oplab services}
  spec.homepage      = "http://www.oplab.com.br"
  spec.license       = "MIT"

  spec.files         = `git ls-files`.split($/)
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.required_ruby_version = '>= 1.9.1'

  spec.add_development_dependency "bundler", "~> 1.3"
  spec.add_development_dependency "rake"
  spec.add_runtime_dependency "rest-client"
end
