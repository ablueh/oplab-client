# Oplab::Client

A ruby client for Oplab

## Installation

Add this line to your application's Gemfile:

    gem 'oplab-client', :git => 'https://ablueh@bitbucket.org/ablueh/oplab-client.git'

And then execute:

    $ bundle

## Usage

O client só tem um método que retorna uma string com um novo access_token de acesso ao Oplab para um usuário específico:

    Oplab::Client.access_token("codigo_da_sua_chave", seu_id_do_usuario, {:name => "opcional: nome do usuário", :delay => "opcional: any"})

Caso o usuário tenha acesso a cotações em tempo real, não passar o parametro "delay"

O método deverá retornar uma string com o valor do access_token que será passado na URL de acesso:

    http://www.bitbolsa.com.br/options/index.aspx?access_token=2cMfQ10Pdq2W9coUEAvl6bxq/oNVlRX2YwEYSGPCvwDq34FPOHQL4zgueEsVMzBK--7zMvGDCo9e3wvCkS6h4V0Q==

Qualquer outra situação o método solta uma exceção.    

## Contributing

1. Fork it
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create new Pull Request
